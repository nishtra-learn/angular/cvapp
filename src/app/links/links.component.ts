import { Component, OnInit } from '@angular/core';
import { CvInfoProviderService } from '../shared/cv-info-provider.service';
//import { CV_INFO_MOCK } from "../shared/cvInfoMock";
import { CvModel } from '../shared/cvModel';

@Component({
  selector: 'app-links',
  templateUrl: './links.component.html',
  styleUrls: ['./links.component.css']
})
export class LinksComponent implements OnInit {
  cvModel: CvModel;

  constructor(private cvInfoProvider: CvInfoProviderService) {
    //this.cvModel = CV_INFO_MOCK;
    this.cvModel = this.cvInfoProvider.cvMock;
   }

  ngOnInit(): void {
  }

}
