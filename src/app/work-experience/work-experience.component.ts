import { Component, OnInit } from '@angular/core';
import { CvInfoProviderService } from '../shared/cv-info-provider.service';
//import { CV_INFO_MOCK } from '../shared/cvInfoMock';
import { CvModel } from '../shared/cvModel';

@Component({
  selector: 'app-work-experience',
  templateUrl: './work-experience.component.html',
  styleUrls: ['./work-experience.component.css']
})
export class WorkExperienceComponent implements OnInit {
  cvModel: CvModel;

  constructor(private cvInfoProvider: CvInfoProviderService) {
    //this.cvModel = CV_INFO_MOCK;
    this.cvModel = this.cvInfoProvider.cvMock;
   }

  ngOnInit(): void {
  }

}
