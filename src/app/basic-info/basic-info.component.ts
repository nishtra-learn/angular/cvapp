import { registerLocaleData } from '@angular/common';
import localeRu from "@angular/common/locales/ru";
import { Component, OnInit } from '@angular/core';
import { CvInfoProviderService } from '../shared/cv-info-provider.service';
//import { CV_INFO_MOCK } from "../shared/cvInfoMock";
import { CvModel } from '../shared/cvModel';
registerLocaleData(localeRu);

@Component({
  selector: 'app-basic-info',
  templateUrl: './basic-info.component.html',
  styleUrls: ['./basic-info.component.css']
})
export class BasicInfoComponent implements OnInit {
  cvModel: CvModel;

  constructor(private cvInfoProvider: CvInfoProviderService) {
    //this.cvModel = CV_INFO_MOCK;
    this.cvModel = this.cvInfoProvider.cvMock;
   }

  ngOnInit(): void {
  }

}
