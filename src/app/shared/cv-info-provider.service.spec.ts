import { TestBed } from '@angular/core/testing';

import { CvInfoProviderService } from './cv-info-provider.service';

describe('CvInfoProviderService', () => {
  let service: CvInfoProviderService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CvInfoProviderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
