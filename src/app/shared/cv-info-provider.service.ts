import { Injectable } from '@angular/core';
import { CvModel } from './cvModel';
import { LinkModel } from './linkModel';

@Injectable({
  providedIn: 'root'
})
export class CvInfoProviderService {
  private _cvMock: CvModel
  public get cvMock(): CvModel {
    return this._cvMock;
  }

  constructor() {
    this._cvMock = new CvModel();
    this._cvMock.firstName = 'Yuri';
    this._cvMock.lastName = 'Kosse';
    this._cvMock.phoneNumbers = ['+380971234567'];
    this._cvMock.address = 'Ukraine, Kryvyi Rih';
    this._cvMock.profilePicUrl = '../../assets/images/anonymous-profile.jpg'
    this._cvMock.dateOfBirth = new Date(1993, 1, 2);
    this._cvMock.workExperience = ['English tutor', 'Copywriter'];
    this._cvMock.skills = ['C++', 'C#', 'WPF', 'ASP.NET', 'HTML', 'CSS', 'JS', 'Node.js'];
    this._cvMock.certificates = ['Microsoft Azure'];
    this._cvMock.links = [new LinkModel('https://gitlab.com/Nishtra', 'Gitlab')];
    this._cvMock.desiredSalary = 9999;
  }
}
