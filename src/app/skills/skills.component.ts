import { Component, OnInit } from '@angular/core';
import { CvInfoProviderService } from '../shared/cv-info-provider.service';
//import { CV_INFO_MOCK } from "../shared/cvInfoMock";
import { CvModel } from '../shared/cvModel';

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.css']
})
export class SkillsComponent implements OnInit {
  cvModel: CvModel;
  skillItemStyles = {
    badge: true,
    'badge-success': true
  }
  certificateItemStyles = {
    badge: true,
    'badge-warning': true
  }

  constructor(private cvInfoProvider: CvInfoProviderService) {
    //this.cvModel = CV_INFO_MOCK;
    this.cvModel = this.cvInfoProvider.cvMock;
   }

  ngOnInit(): void {
  }

}
